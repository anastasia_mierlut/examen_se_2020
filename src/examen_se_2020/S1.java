package examen_se_2020;

public class S1 {

}

class A extends B {
	public static void main(String[] args) {
		//we have inheritance here
	}
}

class B {
	private long t;
	
	public B() {
		
	}
	
	public void displayC (C ceva) {
		//we have dependency here
	}
	
	private D dim; //directed association
}

class C {
	
}

class D {
	public void met1 (int i) {
		
	}
	
	F frame; //aggregation
}

class E {
	public void met2() {
		
	}
	
	public D getDim() {
		return dim;
	}

	private final D dim = new D(); //composition
}

class F {
	
}

package examen_se_2020;

public class S2 implements Runnable {

	public static void main(String[] args) {
		S2 s1 = new S2();
        S2 s2 = new S2();

        Thread t1 = new Thread(s1, "lThread1");
        Thread t2 = new Thread(s2, "lThread2");

        t1.start();
        t2.start();
	}

	@Override
	public void run() {
		Thread t = Thread.currentThread();
        for(int i = 0; i < 10; i++){
              System.out.println(t.getName() + " - " + i);
              try {
                    Thread.sleep(4000);
              } catch (InterruptedException e) {
                    e.printStackTrace();
              }
        }
        System.out.println(t.getName() + " job finalised.");
	}

}

